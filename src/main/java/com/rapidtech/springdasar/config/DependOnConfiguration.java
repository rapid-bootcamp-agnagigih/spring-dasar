package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.model.Bar;
import com.rapidtech.springdasar.model.Foo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;

@Slf4j
@Configuration
public class DependOnConfiguration {
    @Lazy
    @Bean
    @DependsOn(value = {"foo"}) // Foo dibut lebih dulu, kemudian bar
    public Bar bar(){
        log.debug("Creating new Bar");
        return new Bar();
    }

    @Bean
    public Foo foo() {
        log.debug("Creating new Foo");
        return new Foo();
    }
}
