package com.rapidtech.springdasar.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.reactive.context.AnnotationConfigReactiveWebApplicationContext;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

class HelloWorldConfigurationTest {
    @Test
    void applicationContextTest() {
        ApplicationContext context = new AnnotationConfigReactiveWebApplicationContext(HelloWorldConfiguration.class);

        Assertions.assertNotNull(context);
    }
}