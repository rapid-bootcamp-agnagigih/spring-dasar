package com.rapidtech.springdasar.lifeCycle;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import static org.junit.jupiter.api.Assertions.*;

class LifeCycleConfigurationTest {
    private ConfigurableApplicationContext context;
    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext(LifeCycleConfiguration.class);
    }

    @Test
    void lifeCycleTest() {
        Connection connection = context.getBean(Connection.class);
        context.close();
    }

    @Test
    void serverTest() {
        Server server = context.getBean(Server.class);
    }
}